<?php
/**
 * Created by PhpStorm.
 * User: Ksun
 * Date: 21.07.14
 * Time: 0:25
 */
?>
<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
</head>
<body>
    <div id="page">
        <div id="header" class="header">


            <ul id="nav" class="localscroll navigation circle">
                <li>
                    <div class="style">
                        <a href="http://localhost:8000/schedule">
                            <span class="text">Расписание</span>
                        </a>
                    </div>
                    <div class="back_circle"></div>
                </li>
                <li>
                    <div class="style">
                        <a href="http://localhost:8000/photo">
                            <span class="text">Фото</span>
                        </a>
                    </div>
                    <div class="back_circle"></div>
                </li>
                <li>
                    <div class="style">
                        <a href="http://localhost:8000/radio">
                            <span class="text">Радио</span>
                        </a>
                    </div>
                    <div class="back_circle"></div>
                </li>
                <li>
                    <div class="style">
                        <a href="http://localhost:8000/sno">
                            <span class="text">СНО</span>
                        </a>
                    </div>
                    <div class="back_circle"></div>
                </li>
                <li>
                    <div class="style">
                        <a href="http://localhost:8000/studdekan">
                            <span class="text">Студдекан</span>
                        </a>
                    </div>
                    <div class="back_circle"></div>
                </li>
                <li>
                    <div class="style">
                        <a href="http://localhost:8000/prof">
                            <span class="text">Профком</span>
                        </a>
                    </div>
                    <div class="back_circle"></div>
                </li>
            </ul>
        </div>

    </div>

</body>
