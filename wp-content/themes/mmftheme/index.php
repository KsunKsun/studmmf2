<?php
/**
 * Created by PhpStorm.
 * User: Ksun
 * Date: 21.07.14
 * Time: 0:41
 */
    get_header(); ?>

<!--bacground image-->
<div class=imgwrapper>
    <div class="display">
        <img src="wp-content/themes/mmftheme/images/kijvmW4csAc_1.jpg" alt="">
    </div>
</div>

<!-- Link to the news-->
<div class="linkwrapper">
    <span class="newslabel">Новости</span>
    <button class="paginate"><i></i><i></i></button>
</div>

<!-- News-->
<div id="content">
    <div class="slider">
    <?php if (have_posts()) { ?>
        <?php while (have_posts()) { the_post(); ?>
            <div class="post">
                <div class="post-title">
                    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                </div>
                <div class="entry">
                    <?php the_content(); ?>
                </div>
                <div class="post-info">
                    <div class="post-date">
                        <span class="post-month"><?php the_time('M') ?></span>
                        <span class="post-day"><?php the_time('j') ?></span>
                    </div>
                    <span class="post-cat">
                        <a href="#"><?php the_category(', ') ?></a>
                    </span>
                    <span class="post-comments">
                        <?php comments_popup_link('No Comments', '1 Comment', '% Comments'); ?>
                    </span>
                </div>

            </div>
        <?php } ?>
        <div class="nav">
            <span class="previous-entries"><?php next_posts_link('Вперед') ?></span>
            <span class="next-entries"><?php previous_posts_link('Назад') ?></span>
        </div>
    <?php } else { ?>
        <h2>Ничего не найдено</h2>
    <?php } ?>
    </div>
</div>

